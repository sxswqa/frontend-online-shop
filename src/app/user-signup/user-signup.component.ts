import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {FormGroup, FormControl} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {User} from '../models/user';

@Component({
    selector: 'app-user-signup',
    templateUrl: './user-signup.component.html',
    styleUrls: ['./user-signup.component.scss']
})
export class UserSignupComponent implements OnInit {
    f = this.fb.group({
        username: ['', Validators.required],
        email: ['', Validators.required],
        password: ['', Validators.required]
    });

    constructor(private fb: FormBuilder, private service: AuthService) {
    }

    ngOnInit(): void {
    }

    onSubmit(form) {
        this.service.signUp(form.value).subscribe(
            (res: User) => {
                console.log(res, 'succ');
            }
        );
    }

}

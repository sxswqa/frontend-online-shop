import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../models/user';
import {AuthService} from '../services/auth.service';
import {FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
    selector: 'app-user-account',
    templateUrl: './user-account.component.html',
    styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {
    id = JSON.parse(localStorage.getItem('user id'));
    currentUser: User;
    f = this.fb.group({
        first_name: [''],
        last_name: [''],
        email: ['', Validators.required],
    });

    constructor(private userService: UserService, private authService: AuthService, private fb: FormBuilder, private router: Router) {
    }

    ngOnInit(): void {
        this.getInfo(this.id);
    }

    logout() {
        this.authService.logout();
    }

    getInfo(id) {
        this.userService.getUserInfo(id).subscribe((res: User) => {
            this.currentUser = res;
            this.f.patchValue({
                first_name: this.currentUser.first_name,
                last_name: this.currentUser.last_name,
                email: this.currentUser.email
            });
        });
    }

    ucFirst(str) {
        if (!str) {
            return str;
        }

        return str[0].toUpperCase() + str.slice(1);
    }

    updateInfo(form) {
        const first_name = this.ucFirst(form.get('first_name').value);
        const last_name = this.ucFirst(form.get('last_name').value);
        const email = form.get('email').value;
        const data = {first_name, last_name, email};
        this.userService.updateUserInfo(data, this.id).subscribe((res: User) => {
            this.currentUser = res;
            this.f.patchValue({
                username: this.currentUser.username,
                first_name: this.currentUser.first_name,
                last_name: this.currentUser.last_name,
                email: this.currentUser.email
            });
        });
    }

}

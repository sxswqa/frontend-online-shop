import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {ApiService} from './services/api.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ItemsComponent } from './items/items.component';
import { SubcategoriesComponent } from './subcategories/subcategories.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { MainCategoryCatalogViewComponent } from './main-category-catalog-view/main-category-catalog-view.component';
import { MainSubcategoryCatalogViewComponent } from './main-subcategory-catalog-view/main-subcategory-catalog-view.component';
import { FooterComponent } from './footer/footer.component';
import {UserAccountComponent} from './user-account/user-account.component';
import { UserSignupComponent } from './user-signup/user-signup.component';
import { UserSigninComponent } from './user-signin/user-signin.component';
import {UserService} from './services/user.service';
import {AuthService} from './services/auth.service';
import {AuthInterceptor} from './interceptors/auth-interceptor';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        ItemsComponent,
        SubcategoriesComponent,
        ItemDetailComponent,
        MainCategoryCatalogViewComponent,
        MainSubcategoryCatalogViewComponent,
        FooterComponent,
        UserAccountComponent,
        UserSignupComponent,
        UserSigninComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        ApiService,
        UserService,
        AuthService,
        {
            provide : HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi   : true,
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

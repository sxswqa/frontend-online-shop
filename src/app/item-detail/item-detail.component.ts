import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../services/api.service';
import {Item} from '../models/item';

@Component({
    selector: 'app-item-detail',
    templateUrl: './item-detail.component.html',
    styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {

    id: number;
    item: Item;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private service: ApiService) {
        this.id = this.route.snapshot.params.item_id;
    }

    ngOnInit(): void {
        this.getItem(this.id);
    }

    getItem(id) {
        this.service.getItem(id).subscribe((res: Item) => {
            this.item = res;
        });
    }

}

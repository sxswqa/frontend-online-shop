import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ItemsComponent} from './items/items.component';
import {SubCategory} from './models/sub-category';
import {SubcategoriesComponent} from './subcategories/subcategories.component';
import {HeaderComponent} from './header/header.component';
import {ItemDetailComponent} from './item-detail/item-detail.component';
import {MainCategoryCatalogViewComponent} from './main-category-catalog-view/main-category-catalog-view.component';
import {MainSubcategoryCatalogViewComponent} from './main-subcategory-catalog-view/main-subcategory-catalog-view.component';
import {UserAccountComponent} from './user-account/user-account.component';
import {UserSignupComponent} from './user-signup/user-signup.component';
import {UserSigninComponent} from './user-signin/user-signin.component';
import {AuthGuard} from './guards/auth.guard';


const routes: Routes = [
    {path: 'category/:category_id', component: MainCategoryCatalogViewComponent},
    {path: 'category/:category_id/subcategory/:subcategory_id', component: MainSubcategoryCatalogViewComponent},
    {path: 'category/:category_id/item/:item_id', component: ItemDetailComponent},
    {path: 'account', component: UserAccountComponent, canActivate: [AuthGuard]},
    {path: 'signup', component: UserSignupComponent},
    {path: 'signin', component: UserSigninComponent},
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}


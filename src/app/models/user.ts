import {UserProfile} from './user-profile';

export class User {
    id: number;
    username: string;
    email: string;
    password: string;
    first_name: string;
    last_name: string;
    profile: UserProfile[];

    constructor(id: number, username: string, email: string, password: string, first_name: string,
                last_name: string, profile: UserProfile[]) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
        this.profile = profile;
    }
}

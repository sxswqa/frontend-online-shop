import {SubCategory} from './sub-category';

export class Category {
    categoryid: number;
    title: string;
    subcategories: SubCategory[];

    constructor(id: number, title: string, subcategories: SubCategory[]) {
        this.categoryid = id;
        this.title = title;
        this.subcategories = subcategories;
    }
}

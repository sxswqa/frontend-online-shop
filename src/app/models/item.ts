export class Item {
    id: number;
    title: string;
    price: number;
    description: string;
    image: string;
    subcategory: number;

    constructor(id: number, title: string, price: number, description: string, image: string, subcategory: number) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.description = description;
        this.image = image;
        this.subcategory = subcategory;
    }
}


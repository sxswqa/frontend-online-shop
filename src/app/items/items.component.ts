import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../services/api.service';
import {Item} from '../models/item';

@Component({
    selector: 'app-items',
    templateUrl: './items.component.html',
    styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
    id: number;
    items: Item[];
    activeRoute;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private service: ApiService) {
    }

    ngOnInit(): void {
        this.route.paramMap
            .subscribe(params => {
                this.activeRoute = this.route.snapshot.url.join('').toString();
                this.id = +params.get('category_id');
                if (this.activeRoute.indexOf('subcategory') !== -1) {
                    this.getSubCategoryItems(+params.get('subcategory_id'));
                } else {
                    this.getCategoryItems(+params.get('category_id'));
                }
            });

    }

    getCategoryItems(id) {
        this.service.getCategoryItems(id).subscribe((res: Item[]) => {
            this.items = res;
        });
    }

    getSubCategoryItems(id) {
        this.service.getSubCategoryItems(id).subscribe((res: Item[]) => {
            this.items = res;
        });
    }

}

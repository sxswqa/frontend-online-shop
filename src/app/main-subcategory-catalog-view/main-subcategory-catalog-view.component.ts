import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../services/api.service';

@Component({
    selector: 'app-main-subcategory-catalog-view',
    templateUrl: './main-subcategory-catalog-view.component.html',
    styleUrls: ['./main-subcategory-catalog-view.component.scss']
})
export class MainSubcategoryCatalogViewComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {
    }

}

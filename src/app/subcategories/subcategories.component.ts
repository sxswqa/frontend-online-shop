import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {ApiService} from '../services/api.service';
import {SubCategory} from '../models/sub-category';
import {Category} from '../models/category';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-subcategories',
    templateUrl: './subcategories.component.html',
    styleUrls: ['./subcategories.component.scss']
})
export class SubcategoriesComponent implements OnInit {

    id: number;
    subcategories: SubCategory[];

    constructor(private route: ActivatedRoute,
                private router: Router,
                private service: ApiService) {

    }

    ngOnInit(): void {
        this.route.paramMap
            .subscribe(params => {
                this.id = +params.get('category_id');
                this.getSubCategories(+params.get('category_id'));
            });

    }

    getSubCategories(id) {
        this.service.getOneCategory(id).subscribe((res: Category) => {
            this.subcategories = res.subcategories;
        });
    }

}

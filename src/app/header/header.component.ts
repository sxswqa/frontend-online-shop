import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {Category} from '../models/category';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    constructor(private service: ApiService) {
    }

    categories: Category[];
    selectedCategory: Category;
    loggedIn: any;

    ngOnInit(): void {
        this.getCategories();
        this.loggedIn = localStorage.getItem('loggedIn');
        console.log(this.loggedIn);
    }

    getCategories() {
        this.service.getCategories().subscribe(
            (res: Category[]) => {
                this.categories = res;
            },
            error => {
                console.log(error);
            }
        );
    }

    onSelect(category) {
        this.selectedCategory = category;
    }
}

import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {User} from '../models/user';
import {Router} from '@angular/router';

@Component({
    selector: 'app-user-signin',
    templateUrl: './user-signin.component.html',
    styleUrls: ['./user-signin.component.scss']
})
export class UserSigninComponent implements OnInit {
    f = this.fb.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
    });

    constructor(private fb: FormBuilder, private service: AuthService, private router: Router) {
    }

    ngOnInit(): void {
    }

    onSubmit(form) {
        this.service.login(form.value).subscribe(
            success => this.router.navigate(['/account'])
        );
    }
}

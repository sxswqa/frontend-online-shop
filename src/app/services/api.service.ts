import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    baseurl = 'http://127.0.0.1:8000/';
    httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

    constructor(private http: HttpClient) {
        this.getCategories();
    }

    getCategories(): Observable<any> {
        return this.http.get(this.baseurl + 'shop/category/', {headers: this.httpHeaders});
    }

    getOneCategory(id) {
        return this.http.get(this.baseurl + 'shop/category/' + id + '/', {headers: this.httpHeaders});
    }

    updateCategory(category) {
        const body = {id: category.id, title: category.title, subcategories: category.subcategories};
        return this.http.put(this.baseurl + 'shop/category/' + category.id + '/', body, {headers: this.httpHeaders});
    }

    getSubCategories(id) {
        return this.http.get(this.baseurl + 'shop/category/' + id + '/get_list_of_subcategories/', {headers: this.httpHeaders});
    }

    getCategoryItems(id) {
        return this.http.get(this.baseurl + 'shop/category/' + id + '/get_list_of_items_in_category', {headers: this.httpHeaders});
    }

    getSubCategoryItems(id) {
        return this.http.get(this.baseurl + 'shop/subcategory/' + id + '/get_list_of_items_in_subcategory', {headers: this.httpHeaders});
    }

    getItem(id) {
        return this.http.get(this.baseurl + 'shop/item/' + id, {headers: this.httpHeaders});
    }

}

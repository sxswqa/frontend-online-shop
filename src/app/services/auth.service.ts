import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as jwt_decode from 'jwt-decode';
import {tap} from 'rxjs/operators';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public token: string;
    public errors: any = [];

    private baseurl = 'http://127.0.0.1:8000/';
    private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

    constructor(private http: HttpClient) {
    }


    signUp(user) {
        const body = {username: user.username, email: user.email, password: user.password};
        return this.http.post(this.baseurl + 'user/', body, {headers: this.httpHeaders});
    }

    login(user) {
        const body = {username: user.username, password: user.password};
        return this.http.post(this.baseurl + 'api-token-auth/', body, {headers: this.httpHeaders}).pipe(
            tap(
                data => {
                    this.setSession(data);
                },
                err => {
                    this.errors = err.error;
                })
        );
    }

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        } catch (Error) {
            return null;
        }
    }

    private setSession(auth) {
        this.token = auth.token;
        const tokenInfo = this.getDecodedAccessToken(this.token);
        localStorage.setItem('token', this.token);
        localStorage.setItem('expires_at', JSON.stringify(moment.unix(tokenInfo.exp)));
        localStorage.setItem('loggedIn', 'true');
        // localStorage.setItem('username', tokenInfo.username);
        localStorage.setItem('user id', tokenInfo.user_id);
        console.log(localStorage);
    }


    public refreshToken() {
        const body = {token: this.token};
        this.http.post(this.baseurl + 'api-token-refresh/', body, {headers: this.httpHeaders}).pipe(
            tap(
                data => {
                    this.setSession(data);
                },
                err => {
                    this.errors = err.error;
                })
        );
    }

    logout() {
        localStorage.clear();
        localStorage.setItem('loggedIn', 'false');
        console.log(localStorage);
    }

    getExpiration() {
        const expiration = localStorage.getItem('expires_at');
        const expiresAt = JSON.parse(expiration);

        return moment(expiresAt);
    }

    isLoggedIn() {
        return moment().isBefore(this.getExpiration());
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseurl = 'http://127.0.0.1:8000/';
  httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  getUserInfo(id) {
    return this.http.get(this.baseurl + 'user/' + id, {headers: this.httpHeaders});
  }

  updateUserInfo(user, id) {
    const body = {first_name: user.first_name, last_name: user.last_name, email: user.email};
    return this.http.patch(this.baseurl + 'user/' + id + '/', body);
  }
}
